const std = @import("std");
const builtin = @import("builtin");
const process = std.process;
const os = std.os;
const fs = std.fs;
const io = std.io;

const Endian = std.builtin.Endian;

fn EType(comptime e: Endian, comptime T: type) type {
    return packed struct {
        const Self = @This();
        raw_value: T,

        fn val(self: Self) T {
            return std.mem.toNative(T, self.raw_value, e);
        }
    };
}

fn ElfAddr(comptime e: Endian, comptime is_64: bool) type {
    if (is_64)
        return EType(e, u64);
    return EType(e, u32);
}

fn ElfOff(comptime e: Endian, comptime is_64: bool) type {
    return ElfAddr(e, is_64);
}

fn ElfHalf(comptime e: Endian) type {
    return EType(e, u16);
}

fn ElfWord(comptime e: Endian) type {
    return EType(e, u32);
}

fn ElfSword(comptime e: Endian) type {
    return EType(e, i32);
}

fn ElfXword(comptime e: Endian) type {
    return EType(e, u64);
}

const EI_CLASS = 4;
const EI_DATA = 5;
const EI_VERSION = 6;
const EI_NIDENT = 16;

const ELFCLASS32 = 1;
const ELFCLASS64 = 2;

const ELFDATA2LSB = 1;
const ELFDATA2MSB = 2;

fn ElfEhdr(comptime e: Endian, comptime is_64: bool) type {
    return packed struct {
        e_ident: [EI_NIDENT]u8,
        e_type: ElfHalf(e),
        e_machine: ElfHalf(e),
        e_version: ElfWord(e),
        e_entry: ElfAddr(e, is_64),
        e_phoff: ElfOff(e, is_64),
        e_shoff: ElfOff(e, is_64),
        e_flags: ElfWord(e),
        e_ehsize: ElfHalf(e),
        e_phentsize: ElfHalf(e),
        e_phnum: ElfHalf(e),
        e_shentsize: ElfHalf(e),
        e_shnum: ElfHalf(e),
        e_shstrndx: ElfHalf(e),
    };
}

const ET_NONE = 0;
const ET_REL = 1;
const ET_EXEC = 2;
const ET_DYN = 3;
const ET_CORE = 4;
const ET_LOOS = 0xfe00;
const ET_HIOS = 0xfeff;
const ET_LOPROC = 0xff00;
const ET_HIPROC = 0xffff;

fn ElfPhdr(comptime e: Endian, comptime is_64: bool) type {
    if (is_64)
        return packed struct {
            p_type: ElfWord(e),
            p_flags: ElfWord(e),
            p_offset: ElfOff(e, is_64),
            p_vaddr: ElfAddr(e, is_64),
            p_paddr: ElfAddr(e, is_64),
            p_filesz: ElfXword(e),
            p_memsz: ElfXword(e),
            p_align: ElfXword(e),
        };
    return packed struct {
        p_type: ElfWord(e),
        p_offset: ElfOff(e, is_64),
        p_vaddr: ElfAddr(e, is_64),
        p_paddr: ElfAddr(e, is_64),
        p_filesz: ElfWord(e),
        p_memsz: ElfWord(e),
        p_flags: ElfWord(e),
        p_align: ElfWord(e),
    };
}

const PT_NULL = 0;
const PT_LOAD = 1;
const PT_DYNAMIC = 2;
const PT_INTERP = 3;
const PT_NOTE = 4;
const PT_SHLIB = 5;
const PT_PHDR = 6;
const PT_TLS = 7;

const PT_GNU_EH_FRAME = 0x6474e550;
const PT_GNU_STACK = 0x6474e551;
const PT_GNU_RELRO = 0x6474e552;

const PT_LOOS = 0x60000000;
const PT_HIOS = 0x6fffffff;
const PT_LOPROC = 0x70000000;
const PT_HIPROC = 0x7fffffff;

fn ElfShdr(comptime e: Endian, comptime is_64: bool) type {
    if (is_64)
        return packed struct {
            sh_name: ElfWord(e),
            sh_type: ElfWord(e),
            sh_flags: ElfXword(e),
            sh_addr: ElfAddr(e, is_64),
            sh_offset: ElfOff(e, is_64),
            sh_size: ElfXword(e),
            sh_link: ElfWord(e),
            sh_info: ElfWord(e),
            sh_addralign: ElfXword(e),
            sh_entsize: ElfXword(e),
        };
    return packed struct {
        sh_name: ElfWord(e),
        sh_type: ElfWord(e),
        sh_flags: ElfWord(e),
        sh_addr: ElfAddr(e, is_64),
        sh_offset: ElfOff(e, is_64),
        sh_size: ElfWord(e),
        sh_link: ElfWord(e),
        sh_info: ElfWord(e),
        sh_addralign: ElfWord(e),
        sh_entsize: ElfWord(e),
    };
}

const SHF_WRITE = 0x1;
const SHF_ALLOC = 0x2;
const SHF_EXECINSTR = 0x4;
const SHF_MERGE = 0x10;
const SHF_STRINGS = 0x20;
const SHF_INFO_LINK = 0x40;
const SHF_LINK_ORDER = 0x80;
const SHF_OS_NONCONFORMING = 0x100;
const SHF_GROUP = 0x200;
const SHF_MASKOS = 0x0ff00000;
const SHF_MASKPROC = 0xf0000000;

const Elf = struct { data: []const u8 };

const PF_X = 0x1;
const PF_W = 0x2;
const PF_R = 0x4;
const PF_MASKOS = 0x0ff00000;
const PF_MASKPROC = 0xf0000000;

fn printElfTypeName(t: u16, stdout: anytype) !void {
    return switch (t) {
        ET_NONE => stdout.writeAll("NONE (None)\n"),
        ET_REL => stdout.writeAll("REL (Relocatable file)\n"),
        ET_EXEC => stdout.writeAll("EXEC (Executable file)\n"),
        ET_DYN => stdout.writeAll("DYN (Shared object file)\n"),
        ET_CORE => stdout.writeAll("CORE (Core file)\n"),
        ET_LOOS...ET_HIOS => stdout.print("OS Specific: ({})\n", .{t}),
        ET_LOPROC...ET_HIPROC => stdout.print("Processor Specific: ({})\n", .{t}),
        else => stdout.print("<unknown>: %{}", .{t}),
    };
}

const UnknownFormater = struct {
    const Self = @This();
    val: u32,
    pub fn format(
        self: Self,
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        return writer.print("<unknown>: {x}", .{self.val});
    }
};

fn printProgramHeaderTypeName(t: u32, stdout: anytype) !void {
    const fmt = "  {s:<14}";
    try switch (t) {
        PT_NULL => stdout.print(fmt, .{"NULL"}),
        PT_LOAD => stdout.print(fmt, .{"LOAD"}),
        PT_DYNAMIC => stdout.print(fmt, .{"DYNAMIC"}),
        PT_INTERP => stdout.print(fmt, .{"INTERP"}),
        PT_NOTE => stdout.print(fmt, .{"NOTE"}),
        PT_SHLIB => stdout.print(fmt, .{"SHLIB"}),
        PT_PHDR => stdout.print(fmt, .{"PHDR"}),
        PT_GNU_EH_FRAME => stdout.print(fmt, .{"GNU_EH_FRAME"}),
        PT_GNU_STACK => stdout.print(fmt, .{"GNU_STACK"}),
        PT_GNU_RELRO => stdout.print(fmt, .{"GNU_RELRO"}),
        PT_TLS => stdout.print(fmt, .{"TLS"}),
        PT_LOPROC...PT_HIPROC => stdout.print("LOPROC+0x{x}", .{t - PT_LOPROC}),
        else => switch (t) {
            PT_LOOS...PT_HIOS => stdout.print("LOOS+0x{x}", .{t - PT_LOOS}),
            else => {
                const f = UnknownFormater{ .val = t };
                try stdout.print("  {:<14}", .{f});
            },
        },
    };
}

fn printFlagOrSpace(flag: u32, str: []const u8, stdout: anytype) !void {
    if (flag != 0) {
        return stdout.writeAll(str);
    }
    return stdout.writeAll(" ");
}

fn printFlags(flags: u32, stdout: anytype) !void {
    try stdout.writeAll(" ");
    try printFlagOrSpace(flags & PF_R, "R", stdout);
    try printFlagOrSpace(flags & PF_W, "W", stdout);
    try printFlagOrSpace(flags & PF_X, "E", stdout);
    // FIXME: Why can't I use ~ on comptimeint?
    const rest = flags & ~(@as(u32, PF_R | PF_W | PF_X));
    if (rest != 0) {
        return stdout.print("Unknown flags: 0x{x}", .{rest});
    }
}

const ElfError = error{
    PastEnd,
    Empty,
};

fn maybeSlice(comptime T: type, data: []const u8, start: u64, num: u64) ![]const T {
    // FIXME: Can this multiplication overflow?
    const size = num * @sizeOf(T);
    const end = if (std.math.add(u64, start, size)) |res| res else |err| {
        return ElfError.PastEnd;
    };
    if (end > data.len) {
        return ElfError.PastEnd;
    }
    return std.mem.bytesAsSlice(T, data[start..end]);
}

fn printErr(fname: []const u8, msg: []const u8, err: anyerror, stderr: anytype) !void {
    try stderr.print("readelf: Error: {s}: {s}: {}\n", .{ fname, msg, err });
}

fn readelf2(comptime e: Endian, comptime is_64: bool, data: []const u8, stdout: anytype) !void {
    const elf = Elf{ .data = data };
    const header = @ptrCast(*const ElfEhdr(e, is_64), data);
    const phnum = header.e_phnum.val();
    const phoff = header.e_phoff.val();
    const PhdrType = ElfPhdr(e, is_64);
    const program_headers = maybeSlice(PhdrType, data, phoff, phnum) catch |err| {
        // FIXME: use stderr
        // FIXME: get the file name here
        try printErr("fname", "Bad program headers", err, stdout);
        return;
    };

    if (program_headers.len == 0) {
        try stdout.writeAll("\nThere are no program headers in this file.\n");
        return;
    }

    try stdout.writeAll("\nElf file type is ");
    try printElfTypeName(header.e_type.val(), stdout);
    try stdout.print("Entry point 0x{x}\n", .{header.e_entry.val()});
    try stdout.print("There are {} program headers, starting at offset {}\n\n", .{
        phnum,
        phoff,
    });
    try stdout.print("Program Headers:\n", .{});
    const hdr = if (is_64) v: {
        break :v "  Type           Offset   VirtAddr           PhysAddr           FileSiz  MemSiz   Flg Align\n";
    } else v: {
        break :v "  Type           Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align\n";
    };
    try stdout.print(hdr, .{});

    const fmt1 = if (is_64) " 0x{x:0>6}" else " 0x{x:0>5}";
    const fmt2 = if (is_64) " 0x{x:0>16}" else " 0x{x:0>8}";

    for (program_headers) |phdr| {
        const p_type = phdr.p_type.val();
        const p_offset = phdr.p_offset.val();
        const p_filesz = phdr.p_filesz.val();
        try printProgramHeaderTypeName(p_type, stdout);
        try stdout.print(" 0x{x:0>6}", .{p_offset});
        try stdout.print(fmt2, .{phdr.p_vaddr.val()});
        try stdout.print(fmt2, .{phdr.p_paddr.val()});
        try stdout.print(fmt1, .{p_filesz});
        try stdout.print(fmt1, .{phdr.p_memsz.val()});
        try printFlags(phdr.p_flags.val(), stdout);
        const alignment = phdr.p_align.val();
        if (alignment == 0) {
            try stdout.writeAll(" 0");
        } else {
            try stdout.print(" 0x{x}", .{phdr.p_align.val()});
        }
        try stdout.writeAll("\n");

        if (p_type == PT_INTERP) {
            const content = try maybeSlice(u8, data, p_offset, p_filesz);
            const interp = std.mem.span(std.meta.assumeSentinel(content.ptr, 0));
            try stdout.print("      [Requesting program interpreter: {s}]\n", .{interp});
        }
    }

    const ShdrType = ElfShdr(e, is_64);
    const shoff = header.e_shoff.val();
    const shnum = header.e_shnum.val();
    if (shnum == 0)
        return;

    const section_headers = maybeSlice(ShdrType, data, shoff, shnum) catch |err| {
        // FIXME: use stderr
        // FIXME: get the file name here
        try printErr("fname", "Bad section headers", err, stdout);
        return;
    };

    const str_table_index = header.e_shstrndx.val();
    if (str_table_index >= section_headers.len) {
        // FIXME: use stderr
        // FIXME: get the file name here
        // FIXME: pass the right error
        try printErr("fname", "Bad string table", ElfError.PastEnd, stdout);
        return;
    }
    const str_table_header = section_headers[str_table_index];
    const str_start = str_table_header.sh_offset.val();
    const str_table = maybeSlice(u8, data, str_start, str_table_header.sh_size.val()) catch |err| {
        // FIXME: use stderr
        // FIXME: get the file name here
        try printErr("fname", "Bad string table", err, stdout);
        return;
    };

    try stdout.writeAll("\n Section to Segment mapping:\n");
    try stdout.writeAll("  Segment Sections...\n");

    for (program_headers) |phdr, i| {
        const start = phdr.p_vaddr.val();
        const end = start + phdr.p_memsz.val();
        try stdout.print("   {:0>2}     ", .{i});

        for (section_headers) |sec| {
            const addr = sec.sh_addr.val();
            if (addr >= start and addr < end and (sec.sh_flags.val() & SHF_ALLOC) != 0) {
                const sh_name = sec.sh_name.val();
                const zed = std.meta.assumeSentinel(str_table.ptr + sh_name, 0);
                try stdout.print("{s} ", .{std.mem.span(zed)});
            }
        }

        try stdout.writeAll("\n");
    }
}

fn readelf(data: []const u8, stdout: anytype) !void {
    const header = @ptrCast(*const ElfEhdr(Endian.Little, true), data);
    const endian = header.e_ident[EI_DATA];
    const class = header.e_ident[EI_CLASS];
    if (endian != ELFDATA2LSB and endian != ELFDATA2MSB) {
        return stdout.writeAll("Invalid Endian");
    }
    if (class != ELFCLASS32 and class != ELFCLASS64) {
        return stdout.writeAll("Invalid Class");
    }
    if (endian == ELFDATA2LSB) {
        if (class == ELFCLASS32)
            return readelf2(Endian.Little, false, data, stdout);
        return readelf2(Endian.Little, true, data, stdout);
    }
    if (class == ELFCLASS32)
        return readelf2(Endian.Big, false, data, stdout);
    return readelf2(Endian.Big, true, data, stdout);
}

fn mmap(file: fs.File) ![]align(std.mem.page_size) const u8 {
    errdefer file.close();
    const size = try file.getEndPos();
    if (size == 0) {
        return ElfError.Empty;
    }
    const map = try os.mmap(null, size, os.PROT_READ, os.MAP_SHARED, file.handle, 0);
    file.close();
    return map;
}

// FIXME: Why there is no interface for writer?
// FIXME: Pass directory and fname?
fn readelfFile(file: fs.File, stdout: anytype) !void {
    // FIXME: use stderr
    // FIXME: get the file name here
    const map = mmap(file) catch |err| {
        // FIXME: better error message?
        try printErr("fname", "Could not mmap file:", err, stdout);
        return;
    };
    defer os.munmap(map);
    return readelf(map, stdout);
}

test "readelf" {
    const stdout = io.getStdOut().writer();
    var dir = try fs.cwd().openDir("../tests", .{ .iterate = true });
    defer dir.close();

    var pathBuf: [std.os.PATH_MAX]u8 = undefined;
    var path = pathBuf[0..];
    const List = std.ArrayList(u8);

    var iter = dir.iterate();
    while (try iter.next()) |entry| {
        const name = entry.name;
        if (std.mem.endsWith(u8, name, ".out") or std.mem.endsWith(u8, name, ".err"))
            continue;
        try io.getStdOut().writer().print("name = {s}\n", .{name});

        var list = List.init(std.testing.allocator);
        defer list.deinit();
        const file = try dir.openFile(name, .{});
        try readelfFile(file, list.writer());
        const buf = list.toOwnedSlice();
        defer std.testing.allocator.free(buf);

        const out_name = try std.fmt.bufPrint(path, "{s}.out", .{name});
        if (dir.openFile(out_name, .{})) |out_file| {
            const expected = try mmap(out_file);
            defer os.munmap(expected);
            try std.testing.expect(std.mem.eql(u8, expected, buf));
        } else |_| {}
        // FIXME: Check .err and return value
    }

    const fname = "../tests/x86_64.so";
    var list = List.init(std.testing.allocator);
    defer list.deinit();
    const file = try fs.cwd().openFile(fname, .{});
    try readelfFile(file, list.writer());
    const buf = list.toOwnedSlice();
    defer std.testing.allocator.free(buf);

    const out_name = "../tests/x86_64.so.out";
    const out_file = try fs.cwd().openFile(out_name, .{});
    const expected = try mmap(out_file);
    defer os.munmap(expected);

    try std.testing.expect(std.mem.eql(u8, expected, buf));
}

pub fn main() !void {
    const stdout = io.getStdOut().writer();
    var args = process.args();
    _ = args.nextPosix();
    const fname = args.nextPosix() orelse {
        try stdout.print("missing file name\n", .{});
        return;
    };

    const file = try fs.cwd().openFile(fname, .{});
    return readelfFile(file, stdout);
}
