package main

import (
	"fmt"
	"github.com/pkg/diff"
	"github.com/pkg/diff/write"
	"io"
	"os"
	"readelf/elf"
	"readelf/elf_file"
	"strings"
	"testing"
)

func fileContent(fname string) string {
	ret, err := os.ReadFile(fname)
	if err != nil {
		return ""
	}
	return string(ret)
}

func checkEqual(t *testing.T, name string, expected string, actual string) {
	if expected != actual {
		t.Errorf("%s: Incorrect output", name)
		diff.Text("a", "b", expected, actual, os.Stderr, write.TerminalColor())
	}
}

func runReadElf(t *testing.T, args []string, expectedStdout string, expectedStderr string) {
	stdout := &strings.Builder{}
	stderr := &strings.Builder{}
	l := logger{stderr: stderr, stdout: stdout}
	readelf(args, &l)

	name := ""
	if len(args) > 1 {
		name = args[1]
	}

	checkEqual(t, name, expectedStdout, stdout.String())
	checkEqual(t, name, expectedStderr, stderr.String())

	expectedHasError := len(expectedStderr) != 0
	if expectedHasError != l.hasError {
		t.Errorf("%s: Incorrect hasError: %v, expected: %v", name, l.hasError, expectedHasError)
	}
}

func TestMissing(t *testing.T) {
	runReadElf(t, []string{"", "no/such/file"}, "",
		"readelf: Error: no/such/file: open: no such file or directory\n")
}

func TestArgs(t *testing.T) {
	runReadElf(t, []string{""}, "", "readelf: Error: Missing file name\n")
}

func TestMultiple(t *testing.T) {
	a := "tests/x86_64-13.exe"
	b := "tests/x86_64.exe"

	aOut := fileContent(a + ".out")
	bOut := fileContent(b + ".out")
	out := fmt.Sprintf("\nFile: %s\n%s\nFile: %s\n%s", a, aOut, b, bOut)

	aErr := fileContent(a + ".err")
	bErr := fileContent(b + ".err")
	err := aErr + bErr
	runReadElf(t, []string{"", a, b}, out, err)
}

func TestReadelf(t *testing.T) {
	files, err := os.ReadDir("tests/")
	if err != nil {
		t.Error(err)
	}
	for _, entry := range files {
		fname := entry.Name()
		if strings.HasSuffix(fname, ".out") || strings.HasSuffix(fname, ".err") {
			continue
		}

		fullFName := "tests/" + fname
		expectedStdout := fileContent(fullFName + ".out")
		expectedStderr := fileContent(fullFName + ".err")
		runReadElf(t, []string{"", fullFName}, expectedStdout, expectedStderr)
	}
}

func addSeed(f *testing.F, fname string) {
	ef, err := elf_file.Open(fname)
	if err != nil {
		return
	}
	defer ef.Close()
	ef.With(func(inner *elf.Elf) {
		c := append([]byte(nil), inner.Data...)
		f.Add(c)
	})
}

func FuzzElf(f *testing.F) {
	dir := "tests/"
	files, err := os.ReadDir(dir)
	if err != nil {
		f.Error(err)
	}
	for _, entry := range files {
		fname := entry.Name()
		if strings.HasSuffix(fname, ".out") || strings.HasSuffix(fname, ".err") {
			continue
		}

		addSeed(f, dir+fname)
	}

	f.Fuzz(func(t *testing.T, data []byte) {
		e, err := elf.Wrap(data)
		if err == nil {
			l := logger{stderr: io.Discard, stdout: io.Discard}
			readelfElf(&e, &l)
		}
	})
}
