//go:generate go run  golang.org/x/tools/cmd/stringer -type=EType -linecomment

package elf

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"codeberg.org/espindola/assert"
	"reflect"
	"unsafe"
)

func (d *Elf) elfHdrType() reflect.Type {
	if d.Is64Bits {
		return reflect.TypeOf(elf64Ehdr{})
	}
	return reflect.TypeOf(elf32Ehdr{})
}

func knownField(t reflect.Type, fname string) uintptr {
	f, ok := t.FieldByName(fname)
	assert.Assert(ok, "Missing field")
	return f.Offset
}

var NotElf = errors.New("Not an ELF file")
var InvalidClass = errors.New("Invalid EI_CLASS")
var InvalidEndian = errors.New("Invalid EI_DATA")
var PastEnd = errors.New("Extends past the end of file")
var StartsPastEnd = errors.New("Starts past the string table")
var BadIndex = errors.New("Index out of range")

const (
	EI_CLASS   = 4
	EI_DATA    = 5
	EI_VERSION = 6
	EI_NIDENT  = 16
)

const ELFCLASS32 = 1
const ELFCLASS64 = 2

const ELFDATA2LSB = 1
const ELFDATA2MSB = 2

const EV_CURRENT = 1

type elf32Ehdr struct {
	e_ident     [EI_NIDENT]byte
	e_type      elf32Half
	e_machine   elf32Half
	e_version   elf32Word
	e_entry     elf32Addr
	e_phoff     elf32Off
	e_shoff     elf32Off
	e_flags     elf32Word
	e_ehsize    elf32Half
	e_phentsize elf32Half
	e_phnum     elf32Half
	e_shentsize elf32Half
	e_shnum     elf32Half
	e_shstrndx  elf32Half
}

type elf64Ehdr struct {
	e_ident     [EI_NIDENT]byte
	e_type      elf64Half
	e_machine   elf64Half
	e_version   elf64Word
	e_entry     elf64Addr
	e_phoff     elf64Off
	e_shoff     elf64Off
	e_flags     elf64Word
	e_ehsize    elf64Half
	e_phentsize elf64Half
	e_phnum     elf64Half
	e_shentsize elf64Half
	e_shnum     elf64Half
	e_shstrndx  elf64Half
}

func Wrap(data []byte) (Elf, error) {
	const MinSize = unsafe.Sizeof(elf32Ehdr{})
	Size := uintptr(len(data))
	if Size < MinSize || data[0] != 0x7f || data[1] != 'E' || data[2] != 'L' || data[3] != 'F' {
		return Elf{}, NotElf
	}
	class := data[EI_CLASS]
	if class != ELFCLASS32 && class != ELFCLASS64 {
		return Elf{}, InvalidClass
	}
	if class == ELFCLASS64 && Size < unsafe.Sizeof(elf64Ehdr{}) {
		return Elf{}, NotElf
	}
	endian := data[EI_DATA]
	if endian != ELFDATA2LSB && endian != ELFDATA2MSB {
		return Elf{}, InvalidEndian
	}
	var order binary.ByteOrder = binary.BigEndian
	if endian == ELFDATA2LSB {
		order = binary.LittleEndian
	}
	return Elf{data, class == ELFCLASS64, order}, nil
}

type EType uint16

const (
	ET_NONE EType = iota // NONE (None)
	ET_REL               // REL (Relocatable file)
	ET_EXEC              // EXEC (Executable file)
	ET_DYN               // Shared of PIE
	ET_CORE              // CORE (Core file)
)
const (
	ET_LOOS   = 0xfe00
	ET_HIOS   = 0xfeff
	ET_LOPROC = 0xff00
	ET_HIPROC = 0xffff
)

func (e *Elf) hdrFieldOffset(fname string) uintptr {
	return knownField(e.elfHdrType(), fname)
}

func (e *Elf) Type() EType {
	return EType(e.half(e.hdrFieldOffset("e_type")))
}

func (e *Elf) Entry() uint64 {
	return e.addr(e.hdrFieldOffset("e_entry"))
}

func (e *Elf) PHOff() uint64 {
	return e.addr(e.hdrFieldOffset("e_phoff"))
}

func (e *Elf) PHNum() uint16 {
	return e.half(e.hdrFieldOffset("e_phnum"))
}

func (e *Elf) SHOff() uint64 {
	return e.addr(e.hdrFieldOffset("e_shoff"))
}

func (e *Elf) SHNum() uint16 {
	return e.half(e.hdrFieldOffset("e_shnum"))
}

func (e *Elf) SHStrNdx() uint16 {
	return e.half(e.hdrFieldOffset("e_shstrndx"))
}

func CStr(b []byte) ([]byte, error) {
	end := bytes.IndexByte(b, 0)
	if end == -1 {
		return b, fmt.Errorf("String is not null terminated")
	}
	return b[0:end], nil
}
