package elf

import (
	"fmt"
	"reflect"
)

type elf32Dyn struct {
	d_tag elf32Sword
	d_un  elf32Word // d_val or d_ptr
}

type elf64Dyn struct {
	d_tag elf64Sxword
	d_un  elf64Xword // d_val or d_ptr
}

type DynamicSection struct {
	Element
}

const (
	DT_NULL            = 0
	DT_NEEDED          = 1
	DT_PLTRELSZ        = 2
	DT_PLTGOT          = 3
	DT_HASH            = 4
	DT_STRTAB          = 5
	DT_SYMTAB          = 6
	DT_RELA            = 7
	DT_RELASZ          = 8
	DT_RELAENT         = 9
	DT_STRSZ           = 10
	DT_SYMENT          = 11
	DT_INIT            = 12
	DT_FINI            = 13
	DT_SONAME          = 14
	DT_RPATH           = 15
	DT_SYMBOLIC        = 16
	DT_REL             = 17
	DT_RELSZ           = 18
	DT_RELENT          = 19
	DT_PLTREL          = 20
	DT_DEBUG           = 21
	DT_TEXTREL         = 22
	DT_JMPREL          = 23
	DT_BIND_NOW        = 24
	DT_INIT_ARRAY      = 25
	DT_FINI_ARRAY      = 26
	DT_INIT_ARRAYSZ    = 27
	DT_FINI_ARRAYSZ    = 28
	DT_RUNPATH         = 29
	DT_FLAGS           = 30
	DT_ENCODING        = 32
	DT_PREINIT_ARRAY   = 32
	DT_PREINIT_ARRAYSZ = 33
	DT_SYMTAB_SHNDX    = 34

	DT_LOOS    = 0x6000000D
	DT_FLAGS_1 = 0x6FFFFFFB
	DT_HIOS    = 0x6ffff000

	DT_LOPROC = 0x70000000
	DT_HIPROC = 0x7fffffff
)

const (
	DF_1_PIE = 0x08000000
)

func (d *Elf) dynamicSectionType() reflect.Type {
	if d.Is64Bits {
		return reflect.TypeOf(elf64Dyn{})
	}
	return reflect.TypeOf(elf32Dyn{})
}

func (e *Elf) DynamicSection(h ProgramHeader) (DynamicSection, error) {
	size := e.dynamicSectionType().Size()

	// If the file size is not a multiple of the element size we
	// will ignore the tail, which is safe.
	num := h.FileSize() / uint64(size)

	elem, err := FirstElement(e, size, h.Offset(), num)
	if err != nil {
		err = fmt.Errorf("Bad dynamic section: %w", err)
	}

	return DynamicSection{elem}, err
}

func (d *DynamicSection) fieldOffset(fname string) uintptr {
	return knownField(d.data.dynamicSectionType(), fname)
}

func (d *DynamicSection) Tag() uint64 {
	return d.data.addr(0)
}

func (d *DynamicSection) Val() uint64 {
	return d.data.addr(d.fieldOffset("d_un"))
}
