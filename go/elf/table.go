package elf

type Element struct {
	data Elf
	size uintptr
}

func (e *Element) IsValid() bool {
	return len(e.data.Data) != 0
}

func (e *Element) Advance() {
	e.data.Data = e.data.Data[e.size:]
}

func (e *Element) AdvanceN(i uintptr) error {
	start := i * e.size
	if start >= uintptr(len(e.data.Data)) {
		return BadIndex
	}
	e.data.Data = e.data.Data[start:]
	return nil
}

func FirstElement(data *Elf, size uintptr, off uint64, num uint64) (Element, error) {
	// While the multiplication could overflow, we only need to
	// check the addition to make sure the returned vale is safe,
	// which is all that is need for now.
	end := off + num*uint64(size)
	if end > uint64(len(data.Data)) || end < off {
		return Element{}, PastEnd
	}
	e := Element{*data, size}
	e.data.Data = e.data.Data[off:end]
	return e, nil

}
