package elf

import "encoding/binary"

// Used to represent both a full elf file or a section of one
type Elf struct {
	Data     []byte
	Is64Bits bool
	order    binary.ByteOrder
}

// Caller must check if offset is out of range
func (d *Elf) addr(offset uintptr) uint64 {
	data := d.Data[offset:]
	o := d.order
	if d.Is64Bits {
		return o.Uint64(data)
	}
	return uint64(o.Uint32(data))
}

// Caller must check if offset is out of range
func (d *Elf) half(offset uintptr) uint16 {
	data := d.Data[offset:]
	return d.order.Uint16(data)
}

// Caller must check if offset is out of range
func (d *Elf) word(offset uintptr) uint32 {
	data := d.Data[offset:]
	return d.order.Uint32(data)
}

type elf32Addr = uint32
type elf32Off = uint32
type elf32Half = uint16
type elf32Word = uint32
type elf32Sword = int32

type elf64Addr = uint64
type elf64Off = uint64
type elf64Half = uint16
type elf64Word = uint32
type elf64Sword = int32
type elf64Xword = uint64
type elf64Sxword = int64
