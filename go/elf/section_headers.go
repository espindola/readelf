package elf

import (
	"fmt"
	"reflect"
)

type elf32Shdr struct {
	sh_name      elf32Word
	sh_type      elf32Word
	sh_flags     elf32Word
	sh_addr      elf32Addr
	sh_offset    elf32Off
	sh_size      elf32Word
	sh_link      elf32Word
	sh_info      elf32Word
	sh_addralign elf32Word
	sh_entsize   elf32Word
}

type elf64Shdr struct {
	sh_name      elf64Word
	sh_type      elf64Word
	sh_flags     elf64Xword
	sh_addr      elf64Addr
	sh_offset    elf64Off
	sh_size      elf64Xword
	sh_link      elf64Word
	sh_info      elf64Word
	sh_addralign elf64Xword
	sh_entsize   elf64Xword
}

const (
	SHF_WRITE            = 0x1
	SHF_ALLOC            = 0x2
	SHF_EXECINSTR        = 0x4
	SHF_MERGE            = 0x10
	SHF_STRINGS          = 0x20
	SHF_INFO_LINK        = 0x40
	SHF_LINK_ORDER       = 0x80
	SHF_OS_NONCONFORMING = 0x100
	SHF_GROUP            = 0x200
	SHF_MASKOS           = 0x0ff00000
	SHF_MASKPROC         = 0xf0000000
)

type SectionHeader struct {
	Element
}

func (d *Elf) sectionHdrType() reflect.Type {
	if d.Is64Bits {
		return reflect.TypeOf(elf64Shdr{})
	}
	return reflect.TypeOf(elf32Shdr{})
}

func (e *Elf) FirstSectionHeader() (SectionHeader, error) {
	elem, err := FirstElement(e, e.sectionHdrType().Size(), e.SHOff(), uint64(e.SHNum()))
	if err != nil {
		err = fmt.Errorf("Bad section headers: %w", err)
	}
	return SectionHeader{elem}, err
}

func (h *SectionHeader) fieldOffset(fname string) uintptr {
	return knownField(h.data.sectionHdrType(), fname)
}

func (h *SectionHeader) addr(fname string) uint64 {
	o := h.fieldOffset(fname)
	return h.data.addr(o)
}

func (h *SectionHeader) word(fname string) uint32 {
	o := h.fieldOffset(fname)
	return h.data.word(o)
}

func (h *SectionHeader) Offset() uint64 {
	return h.addr("sh_offset")
}

func (h *SectionHeader) Size() uint64 {
	return h.addr("sh_size")
}

func (h *SectionHeader) Addr() uint64 {
	return h.addr("sh_addr")
}

func (h *SectionHeader) Flags() uint64 {
	return h.addr("sh_flags")
}

func (h *SectionHeader) Name() uint32 {
	return h.word("sh_name")
}

func (e *Elf) SectionContent(h SectionHeader) ([]uint8, error) {
	start := h.Offset()
	end := start + h.Size()
	if end > uint64(len(e.Data)) || end < start {
		// The caller knows how to best identify this section in errors, so we just use PastEnd
		return []uint8{}, PastEnd
	}
	return e.Data[start:end], nil
}

func (e *Elf) SectionContentIdx(first SectionHeader, idx uint16) ([]uint8, error) {
	err := first.AdvanceN(uintptr(idx))
	if err != nil {
		return []uint8{}, err
	}
	return e.SectionContent(first)
}

func GetSecName(sec SectionHeader, strTable []byte) ([]byte, error) {
	start := sec.Name()
	if start < uint32(len(strTable)) {
		return CStr(strTable[sec.Name():])
	}
	// The caller knows how to best identify this section in errors, so we just use StartsPastEnd
	return []byte{}, StartsPastEnd
}
