//go:generate go run  golang.org/x/tools/cmd/stringer -type=Header -trimprefix PT_

package elf

import (
	"fmt"
	"reflect"
)

type elf32Phdr struct {
	p_type   elf32Word
	p_offset elf32Off
	p_vaddr  elf32Addr
	p_paddr  elf32Addr
	p_filesz elf32Word
	p_memsz  elf32Word
	p_flags  elf32Word
	p_align  elf32Word
}

type elf64Phdr struct {
	p_type   elf64Word
	p_flags  elf64Word
	p_offset elf64Off
	p_vaddr  elf64Addr
	p_paddr  elf64Addr
	p_filesz elf64Xword
	p_memsz  elf64Xword
	p_align  elf64Xword
}

type Header uint32

const (
	PT_LOOS   = 0x60000000
	PT_HIOS   = 0x6fffffff
	PT_LOPROC = 0x70000000
	PT_HIPROC = 0x7fffffff
)

const (
	PT_NULL Header = iota
	PT_LOAD
	PT_DYNAMIC
	PT_INTERP
	PT_NOTE
	PT_SHLIB
	PT_PHDR
	PT_TLS
)

const (
	PT_GNU_EH_FRAME Header = PT_LOOS + 0x474e550 + iota
	PT_GNU_STACK
	PT_GNU_RELRO
	PT_GNU_PROPERTY
)

const (
	PF_X        = 0x1
	PF_W        = 0x2
	PF_R        = 0x4
	PF_MASKOS   = 0x0ff00000
	PF_MASKPROC = 0xf0000000
)

func (d *Elf) programHdrType() reflect.Type {
	if d.Is64Bits {
		return reflect.TypeOf(elf64Phdr{})
	}
	return reflect.TypeOf(elf32Phdr{})
}

type ProgramHeader struct {
	Element
}

func (e *Elf) FirstProgramHeader() (ProgramHeader, error) {
	elem, err := FirstElement(e, e.programHdrType().Size(), e.PHOff(), uint64(e.PHNum()))
	if err != nil {
		err = fmt.Errorf("Bad program headers: %w", err)
	}
	return ProgramHeader{elem}, err
}

func (h *ProgramHeader) Type() Header {
	return Header(h.data.word(0))
}

func (h *ProgramHeader) fieldOffset(fname string) uintptr {
	return knownField(h.data.programHdrType(), fname)
}

func (h *ProgramHeader) addr(fname string) uint64 {
	o := h.fieldOffset(fname)
	return h.data.addr(o)
}

func (h *ProgramHeader) word(fname string) uint32 {
	o := h.fieldOffset(fname)
	return h.data.word(o)
}

func (h *ProgramHeader) Offset() uint64 {
	return h.addr("p_offset")
}

func (h *ProgramHeader) VAddr() uint64 {
	return h.addr("p_vaddr")
}

func (h *ProgramHeader) PAddr() uint64 {
	return h.addr("p_paddr")
}

func (h *ProgramHeader) FileSize() uint64 {
	return h.addr("p_filesz")
}

func (h *ProgramHeader) MemSize() uint64 {
	return h.addr("p_memsz")
}

func (h *ProgramHeader) Flags() uint32 {
	return h.word("p_flags")
}

func (h *ProgramHeader) Align() uint64 {
	return h.addr("p_align")
}

func (e *Elf) ProgramHeaderContent(h ProgramHeader) ([]uint8, error) {
	start := h.Offset()
	end := start + h.FileSize()
	if end > uint64(len(e.Data)) || end < start {
		// The caller knows how to best identify this section in errors, so we just use PastEnd
		return []uint8{}, PastEnd
	}
	return e.Data[start:end], nil

}
