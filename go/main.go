package main

import (
	"fmt"
	"io"
	"os"
	"codeberg.org/espindola/readelf/elf"
	"codeberg.org/espindola/readelf/elf_file"
)

func elfTypeName(t elf.EType, isPie bool) string {
	switch {
	case t == elf.ET_DYN:
		if isPie {
			return "DYN (Position-Independent Executable file)"
		} else {
			return "DYN (Shared object file)"
		}
	case t >= elf.ET_NONE && t <= elf.ET_CORE:
		return t.String()
	case t >= elf.ET_LOOS && t <= elf.ET_HIOS:
		return fmt.Sprintf("OS Specific: (%x)", uint16(t))
	case t >= elf.ET_LOPROC && t <= elf.ET_HIPROC:
		return fmt.Sprintf("Processor Specific: (%x)", uint16(t))
	}
	return fmt.Sprintf("<unknown>: %x", uint16(t))
}

func programHeaderTypeName(t elf.Header) string {
	switch {
	case t >= elf.PT_NULL && t <= elf.PT_TLS:
		fallthrough
	case t >= elf.PT_GNU_EH_FRAME && t <= elf.PT_GNU_PROPERTY:
		return t.String()
	case t >= elf.PT_LOOS && t <= elf.PT_HIOS:
		return fmt.Sprintf("LOOS+0x%x", uint32(t)-elf.PT_LOOS)
	case t >= elf.PT_LOPROC && t <= elf.PT_HIPROC:
		return fmt.Sprintf("LOPROC+0x%x", uint32(t)-elf.PT_LOPROC)
	}
	return fmt.Sprintf("<unknown>: %x", uint32(t))
}

func flagOrSpace(v uint32, s string) string {
	if v != 0 {
		return s
	}
	return " "
}

type logger struct {
	fname    string
	hasError bool
	stderr   io.Writer
	stdout   io.Writer
}

func (l *logger) msg(format string, values ...interface{}) {
	fmt.Fprintf(l.stdout, format, values...)
}

func (l *logger) failm(msg string, err interface{}, v ...interface{}) bool {
	if err == nil {
		return false
	}
	l.hasError = true
	fmt.Fprintf(l.stderr, "readelf: Error: ")
	if len(l.fname) != 0 {
		fmt.Fprintf(l.stderr, "%s: ", l.fname)
	}
	if len(msg) != 0 {
		fmt.Fprintf(l.stderr, msg, v...)
	}

	// Don't print the path twice
	perr, ok := err.(*os.PathError)
	if ok {
		fmt.Fprintf(l.stderr, "%v: %v\n", perr.Op, perr.Err)
	} else {
		fmt.Fprintln(l.stderr, err)
	}
	return true
}

func (l *logger) fail(err interface{}) bool {
	return l.failm("", err)
}

func flagString(f uint32) (string, error) {
	ret := flagOrSpace(f&elf.PF_R, "R")
	ret += flagOrSpace(f&elf.PF_W, "W")
	ret += flagOrSpace(f&elf.PF_X, "E")
	rest := f &^ (elf.PF_R | elf.PF_W | elf.PF_X)
	if rest != 0 {
		return ret, fmt.Errorf("Unknown flags: 0x%x", rest)
	}
	return ret, nil
}

func readelfOne(fname string, log *logger) {
	ef, err := elf_file.Open(fname)
	if log.fail(err) {
		return
	}
	defer ef.Close()
	readelfElfFile(&ef, log)
}

func readelf(args []string, log *logger) {
	nArgs := len(args)
	if nArgs < 2 {
		log.fail("Missing file name")
		return
	}

	for _, fname := range args[1:] {
		log.fname = fname
		if nArgs > 2 {
			log.msg("\nFile: %s\n", fname)
		}
		readelfOne(fname, log)
	}
}

func readelfElfFile(ef *elf_file.Elf, log *logger) {
	err := ef.With(func(inner *elf.Elf) {
		readelfElf(inner, log)
	})
	log.fail(err)
}

func isPie(e *elf.Elf, log *logger, h elf.ProgramHeader) bool {
	for h.IsValid() && h.Type() != elf.PT_DYNAMIC {
		h.Advance()
	}
	if h.IsValid() {
		d, err := e.DynamicSection(h)
		log.fail(err)
		for d.IsValid() && d.Tag() != elf.DT_FLAGS_1 {
			d.Advance()
		}
		if d.IsValid() {
			return (d.Val() & elf.DF_1_PIE) != 0
		}
	}
	return false
}

func readelfElf(e *elf.Elf, log *logger) {
	firstH, err := e.FirstProgramHeader()
	if log.fail(err) {
		return
	}
	log.msg("\n")
	if !firstH.IsValid() {
		log.msg("There are no program headers in this file.\n")
		return
	}
	t := e.Type()
	// FIXME: isPie walks over the program headers. It might be
	// possible to optimize that with the walk that prints the
	// program headers
	// FIXME: Print error and continue if isPie fails.
	ip := isPie(e, log, firstH)
	log.fail(err)
	log.msg("Elf file type is %s\n", elfTypeName(t, ip))
	log.msg("Entry point 0x%x\n", e.Entry())
	numPH := e.PHNum()
	log.msg("There are %d program headers, starting at offset %d\n", numPH, e.PHOff())
	log.msg("\n")
	log.msg("Program Headers:\n")

	headerFmt := "  %-14s %-8s %-10s %-10s %-7s %-7s %s %s\n"
	phFmt := "  %-14s 0x%06x 0x%08x 0x%08x 0x%05x 0x%05x %s %s\n"
	if e.Is64Bits {
		headerFmt = "  %-14s %-8s %-18s %-18s %-8s %-8s %s %s\n"
		phFmt = "  %-14s 0x%06x 0x%016x 0x%016x 0x%06x 0x%06x %s %s\n"
	}
	log.msg(headerFmt, "Type", "Offset", "VirtAddr", "PhysAddr", "FileSiz", "MemSiz", "Flg", "Align")

	h := firstH
	for h.IsValid() {
		t := h.Type()
		tname := programHeaderTypeName(t)
		o := h.Offset()
		a := h.VAddr()
		p := h.PAddr()
		f := h.FileSize()
		m := h.MemSize()
		flags := h.Flags()
		flagStr, err := flagString(flags)
		log.fail(err)
		align := h.Align()
		var alignStr string
		if align == 0 {
			alignStr = "0"
		} else {
			alignStr = fmt.Sprintf("0x%x", align)
		}
		log.msg(phFmt, tname, o, a, p, f, m, flagStr, alignStr)
		if t == elf.PT_INTERP {
			c, err := e.ProgramHeaderContent(h)
			if err == nil {
				c, err = elf.CStr(c)
			}
			log.failm("Bad program interpreter: ", err)
			if len(c) != 0 {
				log.msg("      [Requesting program interpreter: %s]\n", c)
			}
		}
		h.Advance()
	}

	firstSec, err := e.FirstSectionHeader()
	if log.fail(err) {
		return
	}
	if !firstSec.IsValid() {
		return
	}

	shStrNdx := e.SHStrNdx()
	strTable, err := e.SectionContentIdx(firstSec, shStrNdx)
	if log.failm("Bad string table with index %v: ", err, shStrNdx) {
		return
	}

	log.msg("\n Section to Segment mapping:\n")
	log.msg("  Segment Sections...\n")

	i := 0
	h = firstH
	for h.IsValid() {
		start := h.VAddr()
		end := start + h.MemSize()
		log.msg("   %02d     ", i)

		sec := firstSec
		for sec.IsValid() {
			if sec.Addr() >= start && sec.Addr() < end && sec.Flags()&elf.SHF_ALLOC != 0 {
				name, err := elf.GetSecName(sec, strTable)
				if log.failm("Bad name for section %v: ", err, i) {
					name = []byte("<corrupt>")
				}
				log.msg("%s ", name)
			}
			sec.Advance()
		}

		log.msg("\n")

		i++
		h.Advance()
	}
}

func main() {
	l := logger{stderr: os.Stderr, stdout: os.Stdout}
	readelf(os.Args, &l)
	if l.hasError {
		os.Exit(1)
	}
}
