package elf_file

import (
	"codeberg.org/espindola/mmap"
	"codeberg.org/espindola/readelf/elf"
)

type Elf elf.Elf

func Open(fname string) (Elf, error) {
	data, err := mmap.Map(fname)
	if err != nil {
		return Elf{}, err
	}
	elf, err := elf.Wrap(data)
	if err != nil {
		data.Unmap()
	}
	return Elf(elf), err
}

func (e *Elf) data() mmap.MMap {
	return mmap.MMap(e.Data)
}

func (e *Elf) Close() {
	d := e.data()
	d.Unmap()
	e.Data = nil
}

func (e *Elf) With(f func(inner *elf.Elf)) error {
	return e.data().With(func(data []byte) {
		f((*elf.Elf)(e))
	})
}
