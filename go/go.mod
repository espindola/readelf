module codeberg.org/espindola/readelf

go 1.18

require (
	codeberg.org/espindola/assert v0.2.0
	codeberg.org/espindola/mmap v0.2.0
	github.com/pkg/diff v0.0.0-20210226163009-20ebb0f2a09e
	golang.org/x/tools v0.1.12
)

require (
	github.com/edsrzf/mmap-go v1.1.0 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.0.0-20220927170352-d9d178bc13c6 // indirect
)
